///inventory_add_item(x, y, item)
var xx = argument[0];
var yy = argument[1];
var add_item = argument[2];

// Check for out of range error
if (xx != median(0, xx, Inventory.width) || yy != median(0, yy, Inventory.height)) {
    show_debug_message("The x and y are the range of the inventory");
    exit;
}

// Get the item number
var item_count = Inventory.count[# xx, yy];

// Make sure we have items
if (item_count == 0) {
    // Add the item
    Inventory.box[# xx, yy] = add_item;
    
    // Add 1 from the count
    Inventory.count[# xx, yy] ++;
    
    // Return
    show_debug_message("Item added to at inventory location [" + string(xx) + ", " + string(yy) + "].");
    return true;
} else if (item_count > 0) {
    // No item at location
    show_debug_message("There is already an item at inventory location [" + string(xx) + ", " + string(yy) + "].");
    return false;
} else {
    // Return an error because the item count is less than 0
    show_error("The item count at location [" + string(xx) + ", " + string(yy) + "] is less than 0.", false);
}
