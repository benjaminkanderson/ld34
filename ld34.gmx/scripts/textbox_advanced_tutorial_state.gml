///textbox_advanced_tutorial_state
image_xscale = 1;
next.visible = true;
switch (text_index) {
    case 0:
        if (!instance_exists(FriendUnit)) exit;
        x = smooth_approach(x, FriendUnit.x+xoffset, .1);
        y = smooth_approach(y, FriendUnit.y+yoffset, .1);
        break;
        
    case 1:
        if (!instance_exists(Card)) exit;
        x = smooth_approach(x, Card.x+xoffset, .1);
        y = smooth_approach(y, Card.y+yoffset, .1);
        break;
        
    case 3:
        if (!instance_exists(FoeUnit)) exit;
        x = smooth_approach(x, FoeUnit.x+xoffset, .1);
        y = smooth_approach(y, FoeUnit.y+yoffset, .1);
        // next.visible = false;
        break;
        
    case 4:
        x = smooth_approach(x, room_width-52+xoffset, .1);
        y = smooth_approach(y, 86+yoffset, .1);
        break;
        
    case 5:
        if (!instance_exists(Card)) exit;
        x = smooth_approach(x, Card.x+xoffset, .1);
        y = smooth_approach(y, Card.y+yoffset, .1);
        break;
        
    case 6:
        next.visible = false;
        if (!instance_exists(FriendUnit)) exit;
        x = smooth_approach(x, FriendUnit.x+xoffset, .1);
        y = smooth_approach(y, FriendUnit.y+yoffset, .1);
        break;
}
