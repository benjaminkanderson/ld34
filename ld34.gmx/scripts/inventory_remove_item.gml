///inventory_remove_item(x, y)
var xx = argument[0];
var yy = argument[1];

// Check for out of range error
if (xx != median(0, xx, Inventory.width) || yy != median(0, yy, Inventory.height)) {
    show_debug_message("The x and y are the range of the inventory");
    exit;
}

// Get the item number
var item_count = Inventory.count[# xx, yy];

// Make sure we have items
if (item_count > 0) {
    // Get access to the item
    var return_item = Inventory.box[# xx, yy];
    
    // Remove 1 from the count
    Inventory.count[# xx, yy] --;
    
    // Remove the item from the box
    if (item_count-1 == 0) {
        Inventory.box[# xx, yy] = item.none;
    }
    
    // Return the item back out
    return return_item;
} else if (item_count == 0) {
    // No item at location
    show_debug_message("Could not find an item at inventory location [" + string(xx) + ", " + string(yy) + "].");
    return item.none;
} else {
    // Return an error because the item count is less than 0
    show_error("The item count is less than 0.", false);
}
