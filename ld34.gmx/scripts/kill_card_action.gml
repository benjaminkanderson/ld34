///kill_card_action()
var unit = instance_place(x, y, Unit);
if (unit != noone) {
    with (unit) instance_destroy();
    Deck.draw += 1;
    instance_destroy();
} else {
    state = card_move_to_base_state;
}
Game.card = false;
highlight = false;
