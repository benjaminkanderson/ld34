///create_static_objects()
var obj = 0;
do {
    // Check for parent type
    var parent = object_get_parent(obj)
    if (parent == Static || object_is_ancestor(parent, Static) && !instance_exists(obj)) {
        show_debug_message("Adding static class object "+object_get_name(obj)+"[id = "+string(obj)+"] to the room");
        // Create the object
        instance_create( -100, -100, obj);
    }
    obj++;
} until (!object_exists(obj));
