///textbox_intermediate_tutorial_state();
image_xscale = 1;
next.visible = true;
switch (text_index) {
    case 0:
    case 1:
        if (!instance_exists(FriendUnit)) exit;
        x = smooth_approach(x, FriendUnit.x+xoffset, .1);
        y = smooth_approach(y, FriendUnit.y+yoffset+(text_index*8), .1);
        break;
        
    case 2:
        if (!instance_exists(FoeUnit)) exit;
        x = smooth_approach(x, FoeUnit.x+xoffset, .1);
        y = smooth_approach(y, FoeUnit.y+yoffset, .1);
        break;
        
    case 3:
        if (!instance_exists(Card)) exit;
        x = smooth_approach(x, Card.x+xoffset, .1);
        y = smooth_approach(y, Card.y+yoffset, .1);
        break;
        
    case 4:
        if (!instance_exists(FriendUnit)) exit;
        x = smooth_approach(x, FriendUnit.x+xoffset, .1);
        y = smooth_approach(y, FriendUnit.y+yoffset, .1);
        // next.visible = false;
        break;
        
    case 5:
        image_xscale = -1;
        x = smooth_approach(x, 40-xoffset, .1);
        y = smooth_approach(y, 90+yoffset, .1);
        break;
        
    case 8:
        next.visible = false;
        if (!instance_exists(EndTurnButton)) exit;
        x = smooth_approach(x, EndTurnButton.x+xoffset, .1);
        y = smooth_approach(y, EndTurnButton.y+yoffset, .1);
        break;
}
