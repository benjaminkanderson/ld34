///card_move_to_base_state
var tx;
var ty = 152;
switch (index) {
    case 1:
        tx = 128;
        break;
        
    case 2:
        tx = 160;
        break;
        
    case 3:
        tx = 192;
        break;
}

if (round(x) != tx || round(y) != ty) {
    x = smooth_approach(x, tx, .1);
    y = smooth_approach(y, ty, .1);
} else {
    x = tx;
    y = ty;
    state = card_idle_state;
}
