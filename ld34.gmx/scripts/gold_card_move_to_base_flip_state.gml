///gold_card_move_to_base_flip_state
var tx;
var ty = 152;
switch (index) {
    case 1:
        tx = 96;
        break;
        
    case 2:
        tx = 160;
        break;
        
    case 3:
        tx = 224;
        break;
}

if (ceil(x) != tx || ceil(y) != ty) {
    // Move
    x = smooth_approach(x, tx, .1);
    y = smooth_approach(y, ty, .1);
    
    // Flip
    var total_dis = point_distance(xstart, ystart, tx, ty);
    var dis = point_distance(x, y, tx, ty);
    var xscale = (dis/(total_dis/2)-1)*-1;
    image_xscale = xscale;
} else {
    x = tx;
    y = ty;
    state = gold_card_idle_state;
}
