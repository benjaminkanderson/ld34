///foe_unit_idle_state()
if (!instance_exists(FriendUnit)) exit;
if (Game.turn == ACTION && FriendUnit.attack != defense && FriendUnit.state != friend_unit_attack_state) {
    change_states(foe_unit_attack_state, spr_enemy_tree_attack, 0, ATTACK_SPEED);
}
