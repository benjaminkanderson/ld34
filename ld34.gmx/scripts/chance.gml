///chance(percent)
var percent = argument[0];
return random(1) < percent;
