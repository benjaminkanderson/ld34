///gold_card_action()
var unit = instance_place(x, y, Unit);
if (unit != noone) {
    unit.attack += value;
    unit.defense += value;
    while (FriendUnit.attack > FoeUnit.defense) {
        FriendUnit.defense -= 2;
        FriendUnit.attack = FriendUnit.attack - FoeUnit.defense;
    }
    score += 3;
    score_up = instance_create(x, y, ScoreUp);
    score_up.value = 3;
    score_up.image_index = score_up.value-1;
    instance_destroy();
} else if (place_meeting(x, y, Deck) && Game.trade) {
    Deck.draw += 1;
    Game.last_card_value = value;
    Game.trade = 0;
    if (!Game.card_swapped) {
        ini_open("Data.ini");
        ini_write_real("Save", "Card Swapped", 1);
        ini_close();
        Game.card_swapped = true;
    }
    instance_destroy();
} else {
    state = card_move_to_base_state;
}
Game.card = false;
highlight = false;
