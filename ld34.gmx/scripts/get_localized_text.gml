///get_localized_text(text_id)
var text_id = argument[0];
return Localization.localize[text_id, Localization.language];
