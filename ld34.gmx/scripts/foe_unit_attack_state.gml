///foe_unit_attack_state
if (animation_hit_frame(8)) {
    FriendUnit.defense -= attack;
    Game.screen_shake = true;
    audio_play_sound(snd_attack, 10, false);
    repeat(8) {
        instance_create(FriendUnit.x, FriendUnit.y, Particle);
    }
}

if (animation_end()) {
    Game.screen_shake = false;
    view_xview[0] = 0;
    view_yview[0] = 0;
    change_states(foe_unit_idle_state, spr_enemy_tree_idle, 0, IDLE_SPEED);
    if (!instance_exists(FriendUnit)) exit;
    with (FriendUnit) {
        change_states(friend_unit_attack_state, spr_griffin_attack, 0, ATTACK_SPEED);
    }
}
