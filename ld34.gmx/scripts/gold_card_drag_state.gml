///gold_card_drag_state();
if (mouse_check_button_released(mb_left)) {
    var unit = instance_place(x, y, Unit);
    if (unit != noone) {
        unit.attack += value;
        unit.defense += value;
        instance_destroy();
    } else {
        state = gold_card_move_to_base_state;
    }
    Game.card = false;
    highlight = false;
}

x = smooth_approach(x, mouse_x, .2);
y = smooth_approach(y, mouse_y, .2);
