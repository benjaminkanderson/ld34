///card_idle_state
highlight = position_meeting(mouse_x, mouse_y, id) && Game.card = false && instance_exists(FoeUnit) && Game.turn == FRIEND;
if (highlight && mouse_check_button_pressed(mb_left)) {
    state = card_drag_state;
    Game.card = true;
}
