///friend_unit_idle_state()
if (Game.turn == ACTION) {
    if (!instance_exists(FoeUnit)) exit;
    if (attack == FoeUnit.defense) {
        change_states(friend_unit_attack_state, spr_griffin_attack, 0, ATTACK_SPEED);
    }
}
