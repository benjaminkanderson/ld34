///card_drag_state();
if (mouse_check_button_released(mb_left)) {
    script_execute(action);
}

x = smooth_approach(x, mouse_x, .2);
y = smooth_approach(y, mouse_y, .2);
