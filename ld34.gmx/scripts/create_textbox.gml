///create_textbox(string, speed, x, y, width, height, padding, font);
var xx = argument[2];
var yy = argument[3];

// Create the Textbox object
var textbox = instance_create(xx, yy, Textbox);
with (textbox) {
    text = argument[0];
    text_speed = argument[1];
    width = argument[4];
    height = argument[5];
    padding = argument[6];
    font = argument[7];
    
    draw_set_font(font);
    
    text_length = string_length(text);
    font_size = font_get_size(font);
    text_width = string_width_ext(text, font_size+(font_size/2), width-padding*2);
    text_height = string_height_ext(text, font_size+(font_size/2), width-padding*2);
}
