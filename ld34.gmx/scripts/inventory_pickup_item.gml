///inventory_pickup_item(item)
var add_item = argument[0];

// Check for existing stack
for (var yy = 0; yy < Inventory.height; yy++) {
    for (var xx = 0; xx < Inventory.width; xx++) {
        if (Inventory.box[# xx, yy] == add_item) {
            Inventory.count[# xx, yy]++;
            return true;
        }
    }
}


// If you don't find one add the item to the first free box
for (var yy = 0; yy < Inventory.height; yy++) {
    for (var xx = 0; xx < Inventory.width; xx++) {
        if (Inventory.box[# xx, yy] == item.none) {
            Inventory.box[# xx, yy] = add_item;
            Inventory.count[# xx, yy]++;
            return true;
        }
    }
}

/// Return error
show_debug_message("Inventory has no more space");
return false;
