///card_action()
if (place_meeting(x, y, FriendUnit)) {
    FriendUnit.attack += value;
    while (FriendUnit.attack > FoeUnit.defense) {
        FriendUnit.defense -= 2;
        FriendUnit.attack = FriendUnit.attack - FoeUnit.defense;
    }
    instance_destroy();
    if (instance_exists(TextBox)) {
        TextBox.text_index ++;
        TextBox.text_index = min(TextBox.text_index, TextBox.text_count);
    }
    score += 2;
    score_up = instance_create(x, y, ScoreUp);
    score_up.value = 2;
    score_up.image_index = score_up.value-1;
} else if (place_meeting(x, y, FoeUnit)) {
    FoeUnit.defense += value;
    instance_destroy();
    if (instance_exists(TextBox)) {
        TextBox.text_index ++;
        TextBox.text_index = min(TextBox.text_index, TextBox.text_count);
    }
    score += 2;
    score_up = instance_create(x, y, ScoreUp);
    score_up.value = 2;
    score_up.image_index = score_up.value-1;
} else if (place_meeting(x, y, Deck) && Game.trade) {
    if (room == rm_tutorial) {
        state = card_move_to_base_state;
        Game.card = false;
        highlight = false;
        exit;
    }
    Deck.draw += 1;
    Game.last_card_value = value;
    Game.trade = 0;
    if (!Game.card_swapped) {
        ini_open("Data.ini");
        ini_write_real("Save", "Card Swapped", 1);
        ini_close();
        Game.card_swapped = true;
    }
    instance_destroy();
} else {
    state = card_move_to_base_state;
}
Game.card = false;
highlight = false;
