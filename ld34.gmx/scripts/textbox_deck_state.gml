///textbox_deck_state();
if (instance_exists(Deck)) {
    x = smooth_approach(x, Deck.x+32, .1);
    y = smooth_approach(y, Deck.y+yoffset-16, .1);
}
