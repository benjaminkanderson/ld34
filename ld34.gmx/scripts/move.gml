///move(grid_x, grid_y, speed);
var grid_x = argument[0];
var grid_y = argument[1];
var spd = argument[2];

// Update the grid position
if (Grid.grid[# grid_x, grid_y] == noone) {
    var target_x = grid_x_to_x(grid_x);
    var target_y = grid_y_to_y(grid_y);
    if (x != target_x || y != target_y) {
        x = approach(x, target_x, spd);
        y = approach(y, target_y, spd);
        return true;
    } else {
        Grid.grid[# id.grid_x, id.grid_y] = noone;
        Grid.grid[# grid_x, grid_y] = id;
        id.grid_x = grid_x;
        id.grid_y = grid_y;
        return false;
    }
} else {
    // Break
    show_debug_message("Cannot move to ocupied cell");
    return false;
}
