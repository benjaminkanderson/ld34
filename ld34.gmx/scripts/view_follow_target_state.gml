///view_follow_target_state()
// Fail cases
if (!instance_exists(target)) exit;
if (target == noone) exit;

x = smooth_approach(x, target.x, .1);
y = smooth_approach(y, target.y, .1);
