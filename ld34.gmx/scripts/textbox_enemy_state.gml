///textbox_deck_state();
if (instance_exists(FoeUnit)) {
    x = smooth_approach(x, FoeUnit.x+xoffset+16, .1);
    y = smooth_approach(y, FoeUnit.y+yoffset-8, .1);
}
