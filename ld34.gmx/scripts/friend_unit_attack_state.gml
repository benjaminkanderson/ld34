///friend_unit_attack_state()
if (animation_hit_frame(8)) {
    FoeUnit.defense -= attack;
    Game.screen_shake = true;
    audio_play_sound(snd_attack, 10, false);
    repeat(8) {
        instance_create(FoeUnit.x, FoeUnit.y, Particle);
    }
    if (FoeUnit.defense == 0) {
        score += 5;
        score_up = instance_create(FoeUnit.x, FoeUnit.y, ScoreUp);
        score_up.value = 5;
        score_up.image_index = score_up.value-1;
    }
}

if (animation_end()) {
    change_states(friend_unit_idle_state, spr_griffin_idle, 0, IDLE_SPEED);
    Game.turn = FRIEND;
    Deck.draw = 3-instance_number(Card);
    FriendUnit.attack = 0;
    Game.screen_shake = false;
    view_xview[0] = 0;
    view_yview[0] = 0;
    
    // Tutorial stuff
    if (room == rm_tutorial) {
        switch (Game.turn_number) {
            case 2:
                if (instance_exists(Deck)) {
                    with(Deck) {
                        var card = instance_create(x, y, Card);
                        card.value = 4;
                        card.index = 1;
                    }
                }
                var textbox = instance_create(x, y, TextBox);
                with (textbox) {
                    text[0] = "Great job! Because my attack was equal to the enemy's defense";
                    text[1] = "I didn't take any counter attack damage.";
                    text[2] = "Enemies GROW each turn. This Enemy has a defense of 2.";
                    text[3] = "We don't have a card with a value of 2. But it's okay.";
                    text[4] = "Drag the 4 card over to me.";
                    text[5] = "Ouch! My attack exceeded the enemy's defense";
                    text[6] = "and so my attack was reduced by the enemy's defense value.";
                    text[7] = "I also took a penalty of 2 damage.";
                    text[8] = "Now we are ready to end the turn again.";
                    
                    text_count = array_length_1d(text)-1;
                    state = textbox_intermediate_tutorial_state;
                }
                break;
            case 3:
                if (instance_exists(Deck)) {
                    with(Deck) {
                        var card = instance_create(x, y, Card);
                        card.value = 2;
                        card.index = 1;
                        card = instance_create(x, y, Card);
                        card.value = 5;
                        card.index = 2;
                    }
                }
                var textbox = instance_create(x, y, TextBox);
                with (textbox) {
                    text[0] = "This is the last part of the tutorial.";
                    text[1] = "Our cards don't quite match to get the right attack.";
                    text[2] = "But cards can be used for more than just adding attack.";
                    text[3] = "Drag the 2 card over to the enemy.";
                    text[4] = "Wow! The enemy's defese just increased.";
                    text[5] = "Now our 5 card is just the card we need to defeat this foe.";
                    text[6] = "Drag the 5 card over to me and end the turn.";
                    
                    text_count = array_length_1d(text)-1;
                    state = textbox_advanced_tutorial_state;
                }
                break;
        }
    }
}
