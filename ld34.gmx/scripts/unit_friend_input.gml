///unit_friend_input
if (mouse_check_button_pressed(mb_left)) {
    target_grid_x = x_to_grid_x(mouse_x);
    target_grid_y = y_to_grid_y(mouse_y);
    var target = Grid.grid[# target_grid_x, target_grid_y];
    if (target == id) {
        selected = true;
    }
}

if (selected && mouse_check_button_released(mb_left)) {
    target_grid_x = x_to_grid_x(mouse_x);
    target_grid_y = y_to_grid_y(mouse_y);
    var target = Grid.grid[# target_grid_x, target_grid_y];
    if (target == Unit) {
        if (target.alliance == FOE) {
            action = ATTACK;
        }
    } else if (target == noone) {
        action = MOVE;
    }
    selected = false;
}
