///gold_card_move_to_base_state
var tx;
var ty = 152;
switch (index) {
    case 1:
        tx = 96;
        break;
        
    case 2:
        tx = 160;
        break;
        
    case 3:
        tx = 224;
        break;
}

if (round(x) != tx || round(y) != ty) {
    x = smooth_approach(x, tx, .1);
    y = smooth_approach(y, ty, .1);
} else {
    x = tx;
    y = ty;
    state = gold_card_idle_state;
}
