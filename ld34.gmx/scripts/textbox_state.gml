///textbox_state();
if (!instance_exists(FriendUnit)) exit;
x = smooth_approach(x, FriendUnit.x+xoffset, .1);
y = smooth_approach(y, FriendUnit.y+yoffset, .1);
